## @file 	syn.py
## @author	CYRIL URBAN
## @date	2017-03-31
## @brief	SYN: Syntax highlight in Python 3.6

# imports
import sys
import getopt
import re
import pprint
import copy

# global variables:
formatFilePath = None
inputFilePath = None
outputFilePath = None
isBr = False


##
## @brief	  The main function
##
## @return	 Exit status
##
def main():

	# parameter (argument) processing
	parameterProcessing()

	# open input file
	inputData = openInputFile()

	# open format file
	formattingCommands = openFormatFile()

	# get arrays of regexs and commands
	regulars = getRegulars(formattingCommands)
	commands = getCommands(formattingCommands)

	# converte regex from project regex to Python3 regex
	regulars = convertRegulars(regulars)

	# converte commands from project commands to HTML commands
	commands = getTags(commands)

	# get list of HTML commands (tags) with start and end positions
	HtmlcommandsWithPossitions = findPositions(regulars, commands, inputData)

	# put HTML tags to input
	outputData = putHtmltoInput(HtmlcommandsWithPossitions, inputData)

	# print result data
	printOutputData(outputData)

	# correct exit (with exit code 0)
	sys.exit(0)


##
## @brief      Puts a HTML tags to input string.
##
## @param      html   The list with HTML tags and their position
## @param      input  The input string
##
## @return     Output string with HTML tags
##
def putHtmltoInput(html, input):
	
	# no html tags -> return input
	if (len(html) == 0):
		return input

	# only for better orientation in array
	tag = 0
	numbers = 1

	position = 0
	command = 0
	output = ''

	originalhtml = copy.deepcopy(html)

	# index of input (char) array
	i = 0
	while (i < (len(input))):

		# The end of first putting tag(one set of tag positions).
		if(len(html[command][numbers]) == 0):
			# if is not last tag
			if(((len(html)-1) > command)):		

				# puts remaining characters to output (and then reset and continue to next tag)
				while (i < (len(input))):
					output = output + input[i]				
					i += 1	

				# reset input and go for next tag
				position = 0
				command += 1
				input = output
				output = ''
				i = 0
				# rewrite original
				originalhtml = copy.deepcopy(html)
				continue

			# if is last tag, puts remaining characters to output (end then return)
			else:
				output = output + input[i]				
				i += 1
				continue
	
		# if index in input string == postion of tag -> write this tag
		if(i == html[command][numbers][position]):

			output = output + html[command][tag]
			
			# shift positions
			com = 0
			for x in html:
				# shift start (finding) com. from next command
				if(com <= command):
					com += 1
					continue

				pos = 0
				for y in html[com][numbers]:
					
					# open tag(etc <b>)
					if(com%2 == 0):
						# shift only if puting position is less or equal then finding (original) position
						if(html[command][numbers][position] <= originalhtml[com][numbers][pos]):
							html[com][numbers][pos] += len(html[command][tag])
					# close tag(etc </b>)
					else:
						# shift only if puting position is less or equal then finding (original) position
						if(html[command][numbers][position] < originalhtml[com][numbers][pos]):
							html[com][numbers][pos] += len(html[command][tag])

					# next finding position
					pos += 1

				com += 1

			# remove putting position
			html[command][numbers].remove(html[command][numbers][position])

		# else write input to output (char after char)
		else:
			output = output + input[i]				
			i += 1			

			# tag on the last position
			if(i == (len(input))):
				if(i == html[command][numbers][position]):
					
					# write tag to output
					output = output + html[command][tag]

					# if is not last tag
					if(((len(html)-1) > command)):	
						# reset input and go for next tag
						position = 0
						command += 1
						input = output
						output = ''
						i = 0
						# rewrite original
						originalhtml = copy.deepcopy(html)

	# return output string with HTML tags
	return output


##
## @brief      Gets the HTTML tags from commands.
##
## @param      commandList  The command list
##
## @return     The taglist with (start and end) tags.
##
##             Odd indexes of list are start tags and even indexes of list are
##             end tags.
##
def getTags(commandList):
	
	# The returning taglist with (start and end) tags.
	tagList = []

	# for each line from list
	for commandLine in commandList:
		# reset
		startTagLine = ''
		endTagLine = ''
		# for each command from line
		for command in commandLine:
			# converte command to (start and end) HTTML tag
			if(command == 'bold'):
				startTag = '<b>'
				endTag = '</b>'
			elif(command == 'italic'):
				startTag = '<i>'
				endTag = '</i>'
			elif(command == 'underline'):
				startTag = '<u>'
				endTag = '</u>'
			elif(command == 'teletype'):
				startTag = '<tt>'
				endTag = '</tt>'
			elif ((re.match(r'size:([1-7])', command)) != None):
				size = re.match(r'size:([1-7])', command)
				startTag = '<font size=' + size.group(1) + '>'
				endTag = '</font>'
			elif(re.match('color:[0-9a-fA-F]{6}', command)):
				startTag = '<font color=#' + command[6:] + '>'
				endTag = '</font>'
			else:
				print ("Error in format command!", file=sys.stderr)
				sys.exit(4)

			# more command in one line
			startTagLine = startTagLine + startTag
			# end tags has the reverse order
			endTagLine = endTag + endTagLine
			
		# append to the main tag list
		tagList.append(startTagLine)
		tagList.append(endTagLine)

	# Returns taglist with (start and end) tags
	# Odd indexes of list are start tags and even indexes are end tags
	return tagList


##
## @brief      Find positions of regex
##
## @param      regulars   The regulars
## @param      commands   The commands
## @param      inputData  The input data
##
## @return     List of HTML commands with start and end positions
##
def findPositions(regulars, commands, inputData):	

	# The list of HTML commands with start and end positions
	tagAndPositions = []

	i = 0
	for regex in regulars:
		# reset arrays for each regex
		starts = []
		ends = []

		if(regex == ".+" or regex == ".*"):
			# first position
			starts.append(0)
			# last position
			ends.append(len(inputData))
		else:
			# executes regex over input data and finds position (start and end)
			for position in re.finditer(regex, inputData):
				if(position.start() != position.end()):
					# array of start positions
					starts.append(position.start())
					# array of end positions
					ends.append(position.end())

		# create tmp list with command and start possition
		tmp = []
		tmp.append(commands[i])
		tmp.append(starts)
		# appends to the main list
		tagAndPositions.append(tmp)

		i += 1

		# create tmp list with command and end possition
		tmp = []
		tmp.append(commands[i])
		tmp.append(ends)
		# appends to the main list
		tagAndPositions.append(tmp)

		i += 1

	# return list of HTML commands with start and end positions
	return tagAndPositions


##
## @brief      Converte regex from project (syn) regex to Python3 regex
##
## @param      regexList  The regular expression list
##
## @return     The list of Python3 regex
##
def convertRegulars(regexList):

	# return list
	regexPythonList = []

	# for each regex (from project SYN)
	for regexSyn in regexList:		
		
		regexPython = ''
		negation = ''

		# '['
		bracket = False

		# parsing regex by chars
		i = 0
		while(i < len(regexSyn)):
				
			# negation flag
			if regexSyn[i] == '!':
				# "!" is last or double "^"
				if(i == len(regexSyn)-1) or (negation == '^'):
					print ("Error in regular expression!", file=sys.stderr)
					sys.exit(4)

				negation = '^'

			# if dot -> skip
			elif(regexSyn[i] == '.'):
				# but dot musn't be first, last char or double dot
				if(i == 0 or i == len(regexSyn)-1) or regexSyn[i-1] == '.':
					print ("Error in regular expression!", file=sys.stderr)
					sys.exit(4)

			# special chars after %
			elif (regexSyn[i] == '%'):
				
				if(regexSyn[i] == '%' and i == len(regexSyn)-1 and regexSyn[i] != '%'):
					print ("Error in regular expression!", file=sys.stderr)
					sys.exit(4)
				
				# checks outs of array
				if ((i + 1) < len(regexSyn)):
					i += 1
					# transfares project (syn) regex to python3 regex
					if regexSyn[i] == 's':
						regexPython += '[' + negation + '(\\r\\n|\\n|\\t|\\f|\\v| )' + ']'
					elif regexSyn[i] == 'a':
						if(negation == '^'):
							regexPython += '[[^A-Z]|[A-Z]]'
						else:
							regexPython += '.'
					elif regexSyn[i] == 'd':
						regexPython += '[' + negation + '0-9' + ']'
					elif regexSyn[i] == 'l':
						regexPython += '[' + negation + 'a-z' + ']'
					elif regexSyn[i] == 'L':
						regexPython += '[' + negation + 'A-Z' + ']'
					elif regexSyn[i] == 'w':
						regexPython += '[' + negation + 'A-Za-z' + ']'
					elif regexSyn[i] == 'W':
						regexPython += '[' + negation + 'A-Za-z0-9' + ']'
					elif regexSyn[i] == 't':
						regexPython += '[' + negation + '\\t' + ']'
					elif regexSyn[i] == 'n':
						regexPython += '[' + negation + '(\\r\\n|\\n)' + ']'		
					elif regexSyn[i] == '.':
						regexPython += negation + '\\.'
					elif regexSyn[i] == '|':
						regexPython += negation + '\\|'
					elif regexSyn[i] == '!':
						regexPython += negation + '\\!'
					elif regexSyn[i] == '*':
						regexPython += negation + '\\*'
					elif regexSyn[i] == '+':
						regexPython += negation + '\\+'
					elif regexSyn[i] == '(':
						regexPython += negation + '\\('
					elif regexSyn[i] == ')':
						regexPython += negation + '\\)'
					elif regexSyn[i] == '%':
						regexPython += negation + '\\%'
					# wrong regex
					else:
						print ("Error in regular expression!", file=sys.stderr)
						sys.exit(4)
					
					even = False
					negation = ''
							
			# not special char
			else: 
	
				# \t and \n -> \\t and \\n
				if(regexSyn[i] in ['t', 'n'] and (regexSyn[i-1] == '\\')):
					regexPython += '\\'

				if negation == '^':
					regexPython += '[^'
				
					# must adds '\\' before this chars
					if regexSyn[i] in ['{', '}', '[', ']', '?', '$', '^']:
						regexPython += '\\' + regexSyn[i]
					else:
						regexPython += regexSyn[i]

					bracket = True	
					negation = ''
				else:
					
					# Earlier closing bracket. Etc:  "!"*" -> [^"]*"
					if(regexSyn[i] in ['"', '.', '|', '!', '*', '+'] and bracket):
						regexPython += ']'
						bracket = False

					# must adds '\\' before this chars
					if regexSyn[i] in ['{', '}', '[', ']', '?', '$', '^']:
						regexPython += '\\'

					# wrong regex
					if(regexSyn[i] in ['+', '*', '.', ')', '|'] and regexSyn[i-1] in ['|', '.']):
						print ("Error in regular expression!", file=sys.stderr)
						sys.exit(4)
					if(regexSyn[i] in ['+', '*'] and regexSyn[i-1] in ['+', '*']):
						print ("Error in regular expression!", file=sys.stderr)
						sys.exit(4)
					if(regexSyn[i] in ['.', ')'] and (regexSyn[i-1] == '(')):
						print ("Error in regular expression!", file=sys.stderr)
						sys.exit(4)
					if((regexSyn[i] == '|') and (i == 0)):
						print ("Error in regular expression!", file=sys.stderr)
						sys.exit(4)
					if((regexSyn[i] == '|') and (i == len(regexSyn)-1)):
						print ("Error in regular expression!", file=sys.stderr)
						sys.exit(4)

					# OK -> copy char
					regexPython += regexSyn[i]
			
			# next char				
			i += 1
			
			if(i == len(regexSyn) and bracket):
				regexPython += ']'
				bracket = False


		# check if regex is correct
		try:
			re.compile(regexPython)
		except re.error:
			print ("Error in regular expression!", file=sys.stderr)
			sys.exit(4)

		# appends new regex
		regexPythonList.append(regexPython)

	# return list of regex
	return regexPythonList


##
## @brief	  Gets the array of commands.
##
## @param	  formattingCommands  The formatting commands
##
## @return	 The array of commands.
## 
def getCommands(formattingCommands):

	divided = []
	# raw (with more '\t' and more spaces) list with command
	rawCommandsList = []	
	# final list with commands
	commandsList = []

	# for each line devides regexSyn and command
	i = 0 
	for lines in formattingCommands:
		# divide by '\t'
		divided = formattingCommands[i].partition("\t")
		# appends the second part (command) to the list of commands
		rawCommandsList.append(divided[2])
		# increments (array's) counter
		i += 1
	
	# deletes more '\t' and more spaces from rawCommandsList
	for i in rawCommandsList:
		# deletes '\t' and 'spaces'
		command = re.sub('\t', '', i, count=0)
		command = re.sub(' ', '', command, count=0)
		# deletes ',' and create list for each line
		command = re.findall('[^,]+', command)		
		# appends the final command to the list of commands
		commandsList.append(command)

	return commandsList

	
##
## @brief	  Gets the array of regular expression.
##
## @param	  formattingCommands  The formatting commands
##
## @return	 The array of regular expression.
##
def getRegulars(formattingCommands):

	regexList = []
	divided = []

	# for each line devides regexSyn and command
	i = 0 
	for lines in formattingCommands:
		# divide by '\t'
		divided = formattingCommands[i].partition("\t")
		# appends the first part (regexSyn) to the list of regexSyn
		regexList.append(divided[0])
		# increments (array's) counter
		i += 1
	
	return regexList


##
## @brief	  Prints an output data.
##
## @param	  outputData  The output data
##
def printOutputData(outputData):
	
	# global variable:
	global isBr

	if(isBr == True):
		outputData = re.sub('\n', '<br />\n', outputData, count=0)


	# global variable:
	global outputFilePath

	# output file path does not exist -> print to stdout
	if (outputFilePath == None):
		file = open(1, 'w', encoding='utf-8', closefd=False)
		file.write(outputData)
	
	# else print to file
	else:
		# try create the file
		try:
			file = open(outputFilePath, 'w', encoding='utf-8')
		except:
			print ("Error opening output file!", file=sys.stderr)
			sys.exit(3)

		# print data to file
		file.write(outputData)

		# try close
		try:
			file.close()
		except:
			print ("Error closing output file!", file=sys.stderr)
			sys.exit(3)

##
## @brief	  Opens a format file.
##
## @return	 array of formatting commands
##
def openFormatFile():

	# global variable:
	global formatFilePath

	# array of formatting commands
	formattingCommands = []

	# if path does not exist then return empty formatting commands
	# else formatting commands from file (from formatFilePath)
	if formatFilePath != None:

		# try open
		try:
			file = open(formatFilePath, encoding='utf-8')
		except:
			print ("Error opening format file!", file=sys.stderr)
			sys.exit(2)

		# parsing data to array by lines
		for line in file.readlines():			
			# continue empty lines
			if (len(line) == 1):
				continue
			# each line must contains '\t'
			if (("\t" in line) != True):
				print ("Error in regular expression!", file=sys.stderr)
				sys.exit(4)
			# if line is OK -> appends to array
			formattingCommands.append(line)
		
		# deletes the '\n' from all lines
		formattingCommands = [line.strip() for line in formattingCommands]

		# try close
		try:
			file.close()
		except:
			print ("Error closing format file!", file=sys.stderr)
			sys.exit(2)

	# return
	return formattingCommands


##
## @brief	  Opens an input file.
##
## @return	 Input data.
##
def openInputFile():

	# global variable:
	global inputFilePath

	# read input from stdin
	if inputFilePath == None:
		file = open(0, 'r', encoding='utf-8', closefd=False)
		inputData = file.read()

	# Open input file from file (from inputFilePath)
	else:
		# try open
		try:
			file = open(inputFilePath, encoding='utf-8')
		except:
			print ("Error opening input file!", file=sys.stderr)
			sys.exit(2)

		# read file
		inputData = file.read()

		# try close
		try:
			file.close()
		except:
			print ("Error closing input file!", file=sys.stderr)
			sys.exit(2)

	# return
	return inputData


##
## @brief	  Parameter processing
##
def parameterProcessing():

	# global variables:
	global formatFilePath
	global inputFilePath
	global outputFilePath
	global isBr

	try:
		opts, args = getopt.getopt(sys.argv[1:], "", ["help", "format=", "input=", "output=", "br"])
	except getopt.GetoptError as err:
		# print error message and exit:
		print ("Wrong arguments!", file=sys.stderr)
		sys.exit(1)

	for o, arg in opts:
		if o == "--help":
			helpMessage()
			sys.exit()
		elif o in ("--format"):
			formatFilePath = arg
		elif o in ("--input"):
			inputFilePath = arg
		elif o in ("--output"):
			outputFilePath = arg
		elif o in ("--br"):
			isBr = True
		else:
			print ("Wrong arguments!", file=sys.stderr)
			sys.exit(1)

##
## @brief	  Print help message to std out
##
def helpMessage():
	print("""
 --help			- Show help message
 --format=filename	- Determining a formatting file
 --input=filename	- Determining a input file
 --output=filename	- Determining a output file
 --br			- Adds element <br /> to the end of each line.
""")


if __name__ == "__main__":
	main()