**Syntax highlight**
====================
The script for automatic highlighting specific parts of the text. The script works with table of regular expressions with the required output formatting.